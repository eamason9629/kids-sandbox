import React, { Component } from 'react';

class ImageListItem extends Component {
    render() {
        return (
            <div className="imageLink">
                <a href="#" onClick={this.props.onClick} data-key={this.props.shortName}>{this.props.shortName} - {this.props.numFound}</a>
            </div>
        )
    }
}

export default ImageListItem;
