import _ from 'lodash';
import React, { Component } from 'react';
import ImageListItem from './ImageListItem';
import ImageDetailsList from './ImageDetailsList';
import imageFileService from '../services/ImageFileService';

class ImageManagerView extends Component {
    constructor() {
        super();
        this.state = {
            pwd: '/Users/a1147203/',
            images: {},
            selectedImage: '',
            filters: {
                showOnlyDuplicates: false
            }
        };

        this.findImages = this.findImages.bind(this);
        this.imageDetails = this.imageDetails.bind(this);
        this.deleteImage = this.deleteImage.bind(this);
        this.onDuplicateFilter = this.onDuplicateFilter.bind(this);
    }

    findImages() {
        this.setState({images: imageFileService.mapImages(this.state.pwd, this.state.filters)});
    }

    imageDetails(event) {
        let shortName = event.target.dataset.key;
        this.setState({selectedImage: shortName});
    }

    deleteImage(event) {
        let filePath = event.target.dataset.path;
        imageFileService.deleteImage(filePath);
        this.findImages();
    }

    onDuplicateFilter(event) {
        this.setState({filters: {showOnlyDuplicates: event.target.checked}}, this.findImages);
    }

    render() {
        return (
            <div className="imageManager">
                <div className="buttonRow">
                    <span>Image Manager!</span>
                    <button onClick={this.findImages}>Find Images</button>
                    <span>Filters:</span>
                    <input type="checkbox" onClick={this.onDuplicateFilter} value={this.state.filters.showOnlyDuplicates}></input>Show only duplicates
                </div>
                <div className="imageList">
                    {_.keys(this.state.images).map(image => {
                        return <ImageListItem key={image} shortName={image} numFound={this.state.images[image].length} onClick={this.imageDetails.bind(this)}/>
                    })}
                </div>
                <div className="selectedImage">
                    <ImageDetailsList key={`selectedImage-${this.state.selectedImage}`} images={this.state.images[this.state.selectedImage] || []} onDelete={this.deleteImage.bind(this)} />
                </div>
            </div>
        )
    }
}

export default ImageManagerView;
