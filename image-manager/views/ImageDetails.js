import React, { Component } from 'react';

class ImageDetails extends Component {
    render() {
        return (
            <div className="imageDetails">
                <div className="thumbnail">
                    <img src={`file:\/\/${this.props.name}`}></img>
                </div>
                <div className="imageStats">
                    <span>{this.props.shortName}</span>
                    <span>{this.props.name}</span>
                    <span>{this.props.stats.size} bytes</span>
                </div>
                <button data-path={this.props.name} onClick={this.props.onDelete}>Delete</button>
            </div>
        )
    }
}

export default ImageDetails;
