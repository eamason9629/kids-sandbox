import React, { Component } from 'react';
import ImageDetails from './ImageDetails';

class ImageDetailsList extends Component {
    render() {
        return (
            <div>
                {this.props.images.map(image => {
                    return <ImageDetails key={image.name} onDelete={this.props.onDelete} {...image} />;
                })}
            </div>
        )
    }
}

export default ImageDetailsList;
