import _ from 'lodash';
import fs from 'fs';

const FILE_EXTENSIONS = ['.jpg'];

class ImageFileService {
    duplicateFilter(images, filters) {
        return filters.showOnlyDuplicates ? images.length > 1 : true;
    }

    listImages(here) {
        let result = [];
        let allFiles = fs.readdirSync(here).filter(file => {
            return !file.startsWith('.') && !file.includes(' ');
        }).map(file => {
            let fileName = here.endsWith('/') ? `${here}${file}` : `${here}/${file}`;
            let stats = { isDirectory: () => {return false;}};
            try {
                stats = fs.statSync(fileName);
            } catch(ignore) {}
            return {name: fileName, shortName: file, stats: stats};
        });
        allFiles.forEach(file => {
            if(file.name.endsWith('.jpg')) {
                result.push(file);
            }
        });
        let subDirs = allFiles.filter(file => {
            return file.stats.isDirectory() && !fs.lstatSync(file.name).isSymbolicLink();
        });
        subDirs.forEach(dir => {
            result = _.union(result, this.listImages(dir.name));
        });
        return result;
    }

    applyFilters(imagesMap, filters) {
        let result = {};
        _.keys(imagesMap).forEach(imageKey => {
            if(this.duplicateFilter(imagesMap[imageKey], filters)) {
                result[imageKey] = imagesMap[imageKey];
            }
        });
        return result;
    }

    mapImages(here, filters) {
        let result = {};
        let images = this.listImages(here);
        images.forEach(image => {
            let key = image.shortName;
            if(!result[key]) {
                result[key] = [];
            }
            result[key].push(image);
        });
        result = this.applyFilters(result, filters);
        return result;
    }

    deleteImage(here) {
        fs.unlink(here);
    }
}

export default new ImageFileService();

export {ImageFileService as ImageFileService};