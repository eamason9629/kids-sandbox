import React from 'react';
import ReactDOM from 'react-dom';
import ImageManagerView from './views/ImageManagerView';

ReactDOM.render(
    <ImageManagerView />, document.getElementById('root')
);
