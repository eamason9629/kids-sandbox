import React from 'react';
import ReactDOM from 'react-dom';
import ReverseWordView from './views/ReverseWordView';

ReactDOM.render(
    <ReverseWordView />, document.getElementById('root')
);
