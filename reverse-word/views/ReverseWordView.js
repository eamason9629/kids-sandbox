import React, { Component } from 'react';

class ReverseWordView extends Component {
    constructor() {
        super();
        this.state = {
            theWord: 'a'
        };

        this.calculateResult = this.calculateResult.bind(this);
        this.onTheWordChange = this.onTheWordChange.bind(this);
    }

    render() {
        return (
            <div>
                <span>Reversing words!</span>
                <div>
                    <label htmlFor="theWord">Word to reverse:</label>
                    <input type="text" name="theWord" id="theWord" value={this.state.theWord} onChange={this.onTheWordChange} />
                </div>
                <div>
                    <label htmlFor="result">Calculated Result:</label>
                    <input type="text" name="result" id="result" disabled value={this.calculateResult()} />
                </div>
            </div>
        )
    }

    calculateResult() {
        let result = 'blah blah';
        return result;
    }

    onTheWordChange(event) {
        this.setState({theWord: event.target.value});
    }
}

export default ReverseWordView;
