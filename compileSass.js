const sass = require('node-sass');
const fs = require('file-system');

if(fs.existsSync('css')) {
    console.log('Removing css directory.');
    fs.rmdirSync('css');
} else {
    console.log('Did not find css directory, moving on.')
}
console.log('Creating css directory.');
fs.mkdirSync('css');

console.log('Compiling SASS files.');
fs.recurse('./scss', ['*.scss', '!_*.scss'], function (filepath) {
    sass.render({
            file: filepath
        },
        function (err, result) {
            if (err) {
                console.error(err);
            } else {
                const cssFileName = filepath.replace('scss/', 'css/').replace('.scss', '.css');
                fs.writeFile(cssFileName, result.css, function (error) {
                    if (error) {
                        console.error(error);
                    } else {
                        console.log(`Compiled ${filepath} to ${cssFileName} successfully!`);
                    }
                });
            }
        }
    );
});
