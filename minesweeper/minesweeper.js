import React from 'react';
import ReactDOM from 'react-dom';
import MinesweeperView from './views/MinesweeperView';

ReactDOM.render(
    <MinesweeperView />, document.getElementById('root')
);
