import React, { Component } from 'react';
import SquareView from './SquareView';

class RowView extends Component {
    constructor() {
        super();

//        this.onClearDisplay = this.onClearDisplay.bind(this);
//        this.onNumberPress = this.onNumberPress.bind(this);
//        this.onKeyDown = this.onKeyDown.bind(this);
//        this.onDecimalPress = this.onDecimalPress.bind(this);
//        this.onOperatorPress = this.onOperatorPress.bind(this);
//        this.finishOperation = this.finishOperation.bind(this);
    }

    render() {
        return (
            <div className="mineRow">
                {this.props.row.map((square, yindex) => {
                    return <SquareView key={`${this.props.xindex}${yindex}`} {...square} />
                })}
            </div>
        )
    }

}

export default RowView;
