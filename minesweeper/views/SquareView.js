import React, { Component } from 'react';

class SquareView extends Component {
    className() {
        let result = 'mapSquare';
        result += this.props.selected ? ' selected' : ' unselected';
        result += this.props.mine ? ' mine' : ' safe';
        result += this.props.flag ? ' flag' : '';
        result += ' neighbors-' + this.props.neighborMines;
        return result;
    }

    render() {
        return <button data-x={this.props.xIndex} data-y={this.props.yIndex} onClick={this.props.onClick} onContextMenu={this.props.onRightClick} className={this.className()}>{this.props.neighborMines}</button>
    }
}

export default SquareView;
