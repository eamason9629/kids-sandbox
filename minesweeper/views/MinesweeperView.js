import _ from 'lodash';
import React, { Component } from 'react';
import RowView from './RowView';
import ControlsView from './ControlsView';

const NEW_GAME_STATE = {
   size: 'small',
   difficulty: 'easy',
   moves: 0,
   mineCounter: 0,
   gameOver: false,
   xray: false,
   board: []
};

class MinesweeperView extends Component {
    constructor() {
        super();
        this.state = NEW_GAME_STATE;

        this.state.board = this.generateBoard();

        this.onChangeSize = this.onChangeSize.bind(this);
        this.onSquareClick = this.onSquareClick.bind(this);
        this.onNewGame = this.onNewGame.bind(this);
        this.iterateOnNeighbors = this.iterateOnNeighbors.bind(this);
        this.onChangeXRay = this.onChangeXRay.bind(this);
        this.onRightClick = this.onRightClick.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);
        this.onChangeDifficulty = this.onChangeDifficulty.bind(this);
    }

    componentWillMount() {
        document.addEventListener('keydown', this.onKeyDown, false);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.onKeyDown, false);
    }

    onKeyDown(event) {
        var key = event.key;
        if(key === 'F2') {
            this.onNewGame();
        }
    }

    onChangeSize(option) {
        this.setState({size: option.target.value});
    }

    onChangeDifficulty(option) {
        this.setState({difficulty: option.target.value});
    }

    iterateOnNeighbors(board, x, y, callback) {
        let neighbors = [{x: -1, y: -1},{x: -1, y: 0},{x: -1, y: 1},{x: 0, y: -1},{x: 0, y: 1},{x: 1, y: -1},{x: 1, y: 0},{x: 1, y: 1}]
        neighbors.forEach(neighbor => {
            let neighborX = parseInt(x) + neighbor.x, neighborY = parseInt(y) + neighbor.y;
            if(neighborX >= 0 && neighborY >= 0 && neighborX < board[0].length && neighborY < board.length) {
                callback(board[neighborY][neighborX]);
            }
        });
    }

    clickSquare(board, x, y) {
        let square = board[y][x];
        if(square.selected || this.state.gameOver) {
            return board;
        }

        if(square.mine) {
            if(this.state.moves === 0) {
                board = this.generateBoard(x, y);
                board = this.clickSquare(board, x, y);
            } else {
                square.selected = true;
                this.setState({gameOver: true, xray: true});
            }
        } else {
            square.selected = true;
            if(square.neighborMines === 0) {
                this.iterateOnNeighbors(board, x, y, (neighbor) => {
                    this.clickSquare(board, neighbor.xIndex, neighbor.yIndex)
                });
            }
        }
        return board;
    }

    onSquareClick(event) {
        let x = event.target.dataset.x, y = event.target.dataset.y;
        if(!this.state.gameOver && !this.state.board[y][x].flag) {
            let board = this.state.board.slice(0);
            board = this.clickSquare(board, x, y);
            this.setState({board: board, moves: this.state.moves + 1});
            this.checkForWin(board);
        }
    }

    checkForWin(board) {
        for(let y = 0; y < board.length; y++) {
            for(let x = 0; x < board[0].length; x++) {
                let square = board[y][x];
                if(!square.mine && !square.selected) {
                    return;
                }
            }
        }
        alert('You Win!!!!!1111!!!');
        this.setState({gameOver: true, xray: true});
    }

    onRightClick(event) {
        if(!this.state.gameOver) {
            let board = this.state.board.slice(0);
            let square = board[event.target.dataset.y][event.target.dataset.x];
            if(!square.selected) {
                square.flag = !square.flag;
                this.setState({board: board, moves: this.state.moves + 1, mineCounter: square.flag ? this.state.mineCounter - 1 : this.state.mineCounter + 1});
            }
        }
    }

    markNeighbors(board, x, y) {
        this.iterateOnNeighbors(board, x, y, (neighbor) => {
            neighbor.neighborMines = neighbor.neighborMines + 1;
        });
    }

    placeMines(board) {
       let mineCounter = board.length * board[0].length;
        switch(this.state.difficulty) {
            case 'easiest': mineCounter = mineCounter * .1; break;
            case 'easy': mineCounter = mineCounter * .125; break;
            case 'intermediate': mineCounter = mineCounter * .15; break;
            case 'hard': mineCounter = mineCounter * .20; break;
            case 'impossible': mineCounter = mineCounter * .30; break;
        }

        mineCounter = Math.trunc(mineCounter);

        this.setState({mineCounter: mineCounter});

        while(mineCounter > 0) {
            let x = _.random(0, board[0].length - 1), y = _.random(0, board.length - 1);
            let curSquare = board[y][x];
            if(!curSquare.mine) {
                curSquare.mine = true;
                this.markNeighbors(board, x, y);
                mineCounter--;
            }
        }
    }

    generateBoard() {
        let x, y, board = [];
        switch(this.state.size) {
            case 'small': x = 9; y = 9; break;
            case 'medium': x = 16; y = 16; break;
            case 'large': x = 24; y = 20; break;
            case 'ludacris': x = 50; y = 50; break;
        }

        for(let curY = 0; curY < y; curY++) {
            board[curY] = [];
            for(let curX = 0; curX < x; curX++) {
                board[curY][curX] = {
                    xIndex: curX,
                    yIndex: curY,
                    selected: false,
                    mine: false,
                    flag: false,
                    neighborMines: 0,
                    onClick: this.onSquareClick.bind(this),
                    onRightClick: this.onRightClick.bind(this)
                }
            }
        }
        this.placeMines(board);
        return board;
    }

    onNewGame() {
        this.setState({board: this.generateBoard(), moves: 0, gameOver: false, xray: false});
    }

    onChangeXRay() {
        this.setState({xray: !this.state.xray});
    }

    render() {
        return (
            <div className={this.state.xray ? 'xray' : ''}>
                <span>
                    Minesweeper! {this.state.gameOver ? 'Game Over!!!' : ''}
                </span>
                <ControlsView key={`${this.state.size}${this.state.xray}${this.state.moves}${this.state.mathCounter}`} onNewGame={this.onNewGame.bind(this)} onChangeSize={this.onChangeSize.bind(this)} onChangeDifficulty={this.onChangeDifficulty.bind(this)} onChangeXRay={this.onChangeXRay.bind(this)} {...this.state} />
                <div className="gameBoard">
                    {this.state.board.map((row, xindex) => {
                        return <RowView key={`row${xindex}`} xindex={xindex} row={row} />
                    })}
                </div>
            </div>
        )
    }
}

export default MinesweeperView;
