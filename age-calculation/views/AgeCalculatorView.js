import React, { Component } from 'react';

class AgeCalculatorView extends Component {
    constructor() {
        super();
        this.state = {
            yearTo: 2050,
            yearBorn: 2018
        };

        this.calculateAge = this.calculateAge.bind(this);
        this.onYearToChange = this.onYearToChange.bind(this);
        this.onYearBornChange = this.onYearBornChange.bind(this);
    }

    render() {
        return (
            <div>
                <span>Age Calculator!</span>
                <div>
                    <label htmlFor="yearTo">Year To Calculate To:</label>
                    <input type="number" name="yearTo" id="yearTo" value={this.state.yearTo} onChange={this.onYearToChange} />
                </div>
                <div>
                    <label htmlFor="yearBorn">Year Born:</label>
                    <input type="number" name="yearBorn" id="yearBorn" value={this.state.yearBorn} onChange={this.onYearBornChange} />
                </div>
                <div>
                    <label htmlFor="calculatedAge">Calculated Age:</label>
                    <input type="number" name="calculatedAge" id="calculatedAge" disabled value={this.calculateAge()} />
                </div>
            </div>
        )
    }

    calculateAge() {
        let calculatedAge = 9999;
        return calculatedAge;
    }

    onYearToChange(event) {
        this.setState({yearTo: parseInt(event.target.value)});
    }

    onYearBornChange(event) {
        this.setState({yearBorn: parseInt(event.target.value)});
    }
}

export default AgeCalculatorView;
