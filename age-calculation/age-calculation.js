import React from 'react';
import ReactDOM from 'react-dom';
import AgeCalculatorView from './views/AgeCalculatorView';

ReactDOM.render(
    <AgeCalculatorView />, document.getElementById('root')
);
