import React from 'react';
import ReactDOM from 'react-dom';
import AtlasView from './views/AtlasView';

ReactDOM.render(
    <AtlasView />, document.getElementById('root')
);
