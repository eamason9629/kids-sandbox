import React, { Component } from 'react';

class SquareDetailView extends Component {
    render() {
        return (
            <div className="squareDetails">
                <span className="title">{this.props.selected ? 'Occupied Square' : 'Inspected Square'}</span>
                <span className="coordinates">({this.props.xIndex}, {this.props.yIndex})</span>
                <span className="terrain">{this.props.terrain}</span>
            </div>
        );
    }
}

export default SquareDetailView;
