import React, { Component } from 'react';
import SquareView from './SquareView';

class RowView extends Component {
    render() {
        return (
            <div className="row">
                {this.props.row.map((square) => {
                    return <SquareView key={`${square.xIndex}-${square.yIndex}`} {...square} {...this.props} />
                })}
            </div>
        )
    }
}

export default RowView;
