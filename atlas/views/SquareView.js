import React, { Component } from 'react';

class SquareView extends Component {
    className() {
        let result = 'atlasSquare';
        result += ` ${this.props.terrain}`;
        if(this.props.xPos == this.props.xIndex && this.props.yPos == this.props.yIndex) {
            result += ' selected';
        }
        return result;
    }

    render() {
        return <button data-x={this.props.xIndex} data-y={this.props.yIndex} onClick={this.props.onClick} onContextMenu={this.props.onRightClick} className={this.className()}></button>
    }
}

export default SquareView;
