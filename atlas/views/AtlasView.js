import _ from 'lodash';
import fs from 'fs';
import React, { Component } from 'react';
import RowView from './RowView';
import SquareDetailView from './SquareDetailView';

const NEW_GAME_STATE = {
   moves: 0,
   atlas: [],
   xPos: 0,
   yPos: 0
};

const TERRAINS = ['forest', 'swamp', 'jungle', 'city', 'ocean'];

class AtlasView extends Component {
    constructor() {
        super();
        this.state = NEW_GAME_STATE;
        this.state.atlas = this.openBortanCity();

        this.onSquareClick = this.onSquareClick.bind(this);
        this.onNewGame = this.onNewGame.bind(this);
        this.iterateOnNeighbors = this.iterateOnNeighbors.bind(this);
        this.onRightClick = this.onRightClick.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);
    }

    componentWillMount() {
        document.addEventListener('keydown', this.onKeyDown, false);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.onKeyDown, false);
    }

    onKeyDown(event) {
        var key = event.key.toLowerCase();
        if(key === 'arrowup' || key === 'arrowdown' || key === 'arrowleft' || key === 'arrowright') {
            this.moveDirection(key);
        }
    }

    iterateOnNeighbors(atlas, x, y, callback) {
        let neighbors = [{x: -1, y: -1},{x: -1, y: 0},{x: -1, y: 1},{x: 0, y: -1},{x: 0, y: 1},{x: 1, y: -1},{x: 1, y: 0},{x: 1, y: 1}]
        neighbors.forEach(neighbor => {
            let neighborX = parseInt(x) + neighbor.x, neighborY = parseInt(y) + neighbor.y;
            if(neighborX >= 0 && neighborY >= 0 && neighborX < atlas[0].length && neighborY < atlas.length) {
                callback(atlas[neighborY][neighborX]);
            }
        });
    }

    moveDirection(direction) {
        let destX = this.state.xPos, destY = this.state.yPos;
        let atlas = this.state.atlas.slice(0);
        if(direction === 'arrowup' && this.state.yPos > 0) {
            destY--;
        } else if(direction === 'arrowdown' && this.state.yPos < this.state.atlas.length - 1) {
            destY++;
        } else if(direction === 'arrowleft' && this.state.xPos > 0) {
            destX--;
        } else if(direction === 'arrowright' && this.state.xPos < this.state.atlas[0].length - 1) {
            destX++;
        }
        atlas = this.clickSquare(atlas, destX, destY);
        this.setState({atlas: atlas, moves: this.state.moves + 1, xPos: destX, yPos: destY, inspectSquare: null});
    }

    clickSquare(atlas, x, y) {
        let square = atlas[y][x];
        if(!square.selected) {
            let prevSquare = atlas[this.state.yPos][this.state.xPos];
            prevSquare.selected = false;
            square.selected = true;
        }
        return atlas;
    }

    onSquareClick(event) {
        let x = parseInt(event.target.dataset.x), y = parseInt(event.target.dataset.y);
        let atlas = this.state.atlas.slice(0);
        atlas = this.clickSquare(atlas, x, y);
        this.setState({atlas: atlas, moves: this.state.moves + 1, xPos: x, yPos: y, inspectSquare: null});
    }

    onRightClick(event) {
        let x = parseInt(event.target.dataset.x), y = parseInt(event.target.dataset.y);
        let square = this.state.atlas[y][x];
        this.setState({inspectSquare: square});
    }

    pickTerrain() {
        return _.sample(TERRAINS);
    }

    openBortanCity() {
        let atlas = JSON.parse(fs.readFileSync('./atlas/data/bortanCity.json'));
        atlas.forEach((row, yIndex) => {
            row.forEach((square, xIndex) => {
                square.xIndex = xIndex;
                square.yIndex = yIndex;
                square.onClick = this.onSquareClick.bind(this);
                square.onRightClick = this.onRightClick.bind(this);
            });
        });
        return atlas;
    }

    generateAtlas() {
        let x = 100, y = 100, atlas = [];

        for(let curY = 0; curY < y; curY++) {
            atlas[curY] = [];
            for(let curX = 0; curX < x; curX++) {
                atlas[curY][curX] = {
                    xIndex: curX,
                    yIndex: curY,
                    terrain: this.pickTerrain(),
                    onClick: this.onSquareClick.bind(this),
                    onRightClick: this.onRightClick.bind(this)
                }
            }
        }
        return atlas;
    }

    onNewGame() {
        this.setState({atlas: this.generateAtlas()});
    }

    reducedAtlas() {
        let offset = 5;
        let minX = this.state.xPos - offset, maxX = this.state.xPos + offset;
        let minY = this.state.yPos - offset, maxY = this.state.yPos + offset;

        if(minX < 0) minX = 0;
        if(minY < 0) minY = 0;
        if(maxX >= this.state.atlas.length) maxX = this.state.atlas.length - 1;
        if(maxY >= this.state.atlas[0].length) maxY = this.state.atlas[0].length - 1;

        let result = []
        for(let y = minY; y <= maxY; y++) {
            let row = [];
            for(let x = minX; x <= maxX; x++) {
                row.push(this.state.atlas[y][x]);
            }
            result.push(row);
        }

        return result;
    }

    inspectView() {
        let inspectSquare = this.state.inspectSquare;
        if(inspectSquare) {
            return <SquareDetailView key={`inspect-${inspectSquare.yIndex}-${inspectSquare.xIndex}`} {...inspectSquare} />
        }
    }

    render() {
        let square = this.state.atlas[this.state.yPos][this.state.xPos];
        let atlas = this.reducedAtlas();

        return (
            <div>
                <span>
                    Uroth!
                </span>
                <div className="atlas">
                    {atlas.map((row, yIndex) => {
                        return <RowView key={`row${yIndex}`} row={row} {...this.state} />
                    })}
                </div>
                <div className="squareDetailsContainer">
                    <SquareDetailView key={`detail-${this.state.yPos}-${this.state.xPos}`} {...square} />
                    {this.inspectView()}
                </div>
            </div>
        )
    }
}

export default AtlasView;
