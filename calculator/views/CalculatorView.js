import React, { Component } from 'react';

class CalculatorView extends Component {
    constructor() {
        super();
        this.state = {
            display: 0,
            currentOperation: '',
            hangingDecimal: false
        };

        this.onClearDisplay = this.onClearDisplay.bind(this);
        this.onNumberPress = this.onNumberPress.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);
        this.onDecimalPress = this.onDecimalPress.bind(this);
        this.onOperatorPress = this.onOperatorPress.bind(this);
        this.finishOperation = this.finishOperation.bind(this);
    }

    componentWillMount() {
        document.addEventListener('keydown', this.onKeyDown, false);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.onKeyDown, false);
    }

    render() {
        return (
            <div>
                <span>Calculator!</span>
                <div className="display-row">
                    <input type="text" disabled="true" name="operator" id="operator" value={this.state.currentOperation} />
                    <input type="number" disabled="true" name="display" id="display" value={this.state.display} />
                </div>
                <div className="button-row">
                    <button name="clearDisplay" id="clearDisplay" onClick={this.onClearDisplay}>C</button>
                    <button id="invertSign" data-value="invertSign" onClick={this.onOperatorPress}>+/-</button>
                    <button data-value="%" onClick={this.onOperatorPress}>%</button>
                    <span className="divider">|</span>
                    <button data-value="/" onClick={this.onOperatorPress}>/</button>
                </div>
                <div className="button-row">
                    <button data-value="7" onClick={this.onNumberPress}>7</button>
                    <button data-value="8" onClick={this.onNumberPress}>8</button>
                    <button data-value="9" onClick={this.onNumberPress}>9</button>
                    <span className="divider">|</span>
                    <button data-value="*" onClick={this.onOperatorPress}>x</button>
                </div>
                <div className="button-row">
                    <button data-value="4" onClick={this.onNumberPress}>4</button>
                    <button data-value="5" onClick={this.onNumberPress}>5</button>
                    <button data-value="6" onClick={this.onNumberPress}>6</button>
                    <span className="divider">|</span>
                    <button data-value="-" onClick={this.onOperatorPress}>-</button>
                </div>
                <div className="button-row">
                    <button data-value="1" onClick={this.onNumberPress}>1</button>
                    <button data-value="2" onClick={this.onNumberPress}>2</button>
                    <button data-value="3" onClick={this.onNumberPress}>3</button>
                    <span className="divider">|</span>
                    <button data-value="+" onClick={this.onOperatorPress}>+</button>
                </div>
                <div className="button-row">
                    <button data-value="0" className="doubleWide" onClick={this.onNumberPress}>0</button>
                    <button onClick={this.onDecimalPress}>.</button>
                    <span className="divider">|</span>
                    <button data-value="=" onClick={this.finishOperation}>=</button>
                </div>
            </div>
        )
    }

    onKeyDown(event) {
        var key = event.key;
        if(key >= '0' && key <= '9') {
            if(this.state.hangingDecimal) {
                this.concatDigit(`.${key}`);
            } else {
                this.concatDigit(key);
            }
        } else if(key === '.') {
            this.onDecimalPress();
        } else if(key === '=' || key == 'Enter') {
            this.finishOperation();
        } else if(key === '/' || key === '*' || key === '-' || key === '+' || key === '%') {
            this.doOperation(key);
        } else if(key === 'Escape' || key === 'c' || key === 'C') {
            this.onClearDisplay();
        }
    }

    onKeyDownSwitch(event) {
        var key = event.key;
        switch(key) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                if(this.state.hangingDecimal) {
                    this.concatDigit(`.${key}`);
                } else {
                    this.concatDigit(key);
                }
                break;
            case '.':
                this.onDecimalPress(); break;
            case '=':
            case 'Enter':
                this.finishOperation(); break;
            case 'Escape':
            case 'c':
            case 'C':
                this.onClearDisplay(); break;
            case '/':
            case '*':
            case '-':
            case '+':
            case '%':
            default:
                this.doOperation(key);
        }
    }

    onClearDisplay() {
        this.setState({display: 0});
    }

    onNumberPress(button) {
        this.concatDigit(button.target.dataset.value);
    }

    finishOperation() {
        let result = this.state.operator;
        switch(this.state.currentOperation) {
            case '/': result = result / this.state.display; break;
            case '*': result = result * this.state.display; break;
            case '-': result = result - this.state.display; break;
            case '+': result = result + this.state.display; break;
            default: console.error(`Got an operator we don't know about: ${this.state.currentOperation}`);
        }
        this.setState({operator: 0, currentOperation: '', display: result, hangingDecimal: false})
    }

    doOperation(operation) {
        if(operation === 'invertSign') {
            this.setState({display: parseFloat(this.state.display) * -1});
        } else if(operation === '%') {
            this.setState({display: parseFloat(this.state.display) / 100});
        } else {
            this.setState({operator: this.state.display, display: 0, currentOperation: operation});
        }
    }

    onOperatorPress(button) {
        this.doOperation(button.target.dataset.value);
    }

    onDecimalPress() {
        if((this.state.display + '').indexOf('.') === -1) {
            this.setState({hangingDecimal: true});
        }
    }

    concatDigit(number) {
        this.setState({display: parseFloat(this.state.display + `${number}`), hangingDecimal: false});
    }
}

export default CalculatorView;
