import React from 'react';
import ReactDOM from 'react-dom';
import CalculatorView from './views/CalculatorView';

ReactDOM.render(
    <CalculatorView />, document.getElementById('root')
);
