import React, { Component } from 'react';

class NumberMathView extends Component {
    constructor() {
        super();
        this.state = {
            firstNumber: 1,
            secondNumber: 1
        };

        this.onFirstNumberChange = this.onFirstNumberChange.bind(this);
        this.onSecondNumberChange = this.onSecondNumberChange.bind(this);
    }

    render() {
        return (
            <div>
                <div>Number Math!</div>
                <div>
                    <label htmlFor="firstNumber">First Number:</label>
                    <input type="text" name="firstNumber" id="firstNumber" value={this.state.firstNumber} onChange={this.onFirstNumberChange} />
                </div>
                <div>
                    <label htmlFor="secondNumber">Second Number:</label>
                    <input type="text" name="secondNumber" id="secondNumber" value={this.state.secondNumber} onChange={this.onSecondNumberChange} />
                </div>
                <div>{this.state.firstNumber} plus {this.state.secondNumber} equals {0}</div>
                <div>{this.state.firstNumber} minus {this.state.secondNumber} equals {0}</div>
                <div>{this.state.firstNumber} times {this.state.secondNumber} equals {0}</div>
                <div>{this.state.firstNumber} divided by {this.state.secondNumber} equals {0}</div>
            </div>
        )
    }

    onFirstNumberChange(event) {
        this.setState({firstNumber: parseInt(event.target.value)});
    }

    onSecondNumberChange(event) {
        this.setState({secondNumber: parseInt(event.target.value)});
    }
}

export default NumberMathView;
