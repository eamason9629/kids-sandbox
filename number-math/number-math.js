import React from 'react';
import ReactDOM from 'react-dom';
import NumberMathView from './views/NumberMathView';

ReactDOM.render(
    <NumberMathView />, document.getElementById('root')
);
